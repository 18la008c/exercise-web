# exercise-web

## how to use
1. make `.env` file (such as `env.sample`)
2. start container
- dev (flask-mysql)
```
docker-compose up -d --build
```
- prod (nginx-uwsgi-flask-mysql)
```
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d --build
```
3. access to `http://<global ip>:50002`

## option
- test in app container
```
python -m pytest
```

- cicdtest