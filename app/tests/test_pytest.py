import pytest
from app import app
import json
from datetime import datetime


def test_simple():
    actual = 1
    expected = 1
    assert expected == actual


def test_api():
    with app.test_client() as c:
        res = c.get("/api")
        assert res.get_data(as_text=True) == "apiの一覧とhow to useを書きたいです。"


def test_api_memo_post(reset_db):
    with app.test_client() as c:
        res = c.post("/api/memo", data=json.dumps({
            "memo": "test-dayo-"
        }
        ),
            headers={
            "Content-Type": "application/json"
        }
        )
        assert res.get_data(
            as_text=True) == f"{datetime.now():%Y-%m-%d}, test-dayo-"


def test_api_memo_get(reset_db):
    with app.test_client() as c:
        res = c.get("/api/memo")
        assert res.get_json()[0]["memo"] == "test-dayo-"


def test_api_memo_post_error(reset_db):
    with app.test_client() as c:
        res = c.post("/api/memo", data=json.dumps({
            "error": "test-dayo-"
        }
        ),
            headers={
            "Content-Type": "application/json"
        }
        )
        assert res.get_json()["message"] == "parameter error"
        assert res.status_code == 400
