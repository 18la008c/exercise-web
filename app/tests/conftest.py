from mysql import Mysql
import pytest


class TestMysql(Mysql):
    """add clear method for test"""
    def clear_all_data(self):
        sql = "DELETE FROM memo"
        self.cur.execute(sql)
        self.conn.commit()


@pytest.fixture(scope="session")
def reset_db():
    yield
    db = TestMysql("test")
    db.clear_all_data()
