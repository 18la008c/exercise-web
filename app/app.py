from datetime import datetime

from flask import Flask, render_template, request, jsonify, abort
from mysql import Mysql

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

ROWS_NUM_PER_PAGE = 10


@app.route("/", methods=["GET", "POST"])
def index():
    db = Mysql("test")
    res = {}

    if request.method == "POST":
        memo = request.form.get("memo", "")
        if memo:
            created_at = f"{datetime.now():%Y-%m-%d}"
            db.save(created_at, memo)

    data = db.load()[::-1]

    page = request.args.get("page", 1, type=int)
    res["page"] = page
    if page > 1:
        res["previous_page"] = page - 1
    if page * ROWS_NUM_PER_PAGE < len(data):
        res["next_page"] = page + 1

    start_row_no = (page - 1) * ROWS_NUM_PER_PAGE
    res["list"] = data[start_row_no: (start_row_no + ROWS_NUM_PER_PAGE)]
    return render_template("index.html", res=res)


@app.route("/api", methods=["GET"])
def api():
    return "apiの一覧とhow to useを書きたいです。"


@app.route("/api/memo", methods=["GET", "POST"])
def api_memo():
    db = Mysql("test")
    if request.method == "POST":
        try:
            memo = request.json["memo"]
            created_at = f"{datetime.now():%Y-%m-%d}"
            db.save(created_at, memo)
            return f"{created_at}, {memo}"
        except Exception:
            abort(400)
    return jsonify(db.load()[::-1])


@app.errorhandler(404)
def page_not_found(error):
    return "page not found"


@app.errorhandler(400)
def parameter_error(error):
    return jsonify({"message": "parameter error"}), 400


@app.route("/notfound", methods=["GET", "POST"])
def raise_404():
    abort(404)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
