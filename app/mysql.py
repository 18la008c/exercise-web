import os
import MySQLdb

HOST = os.environ["MYSQL_HOST"]
PASS = os.environ["MYSQL_ROOT_PASSWORD"]


class Mysql:
    def __init__(self, db_name, host=HOST, passwd=PASS):
        self.conn = MySQLdb.connect(user="root",
                                    passwd=passwd,
                                    host=host,
                                    db=db_name,
                                    charset="utf8mb4")
        self.cur = self.conn.cursor(MySQLdb.cursors.DictCursor)

    def load(self, sql="SELECT * FROM memo"):
        self.cur.execute(sql)
        return self.cur.fetchall()

    def save(self, created_at, memo):
        sql = f"INSERT INTO memo VALUES('{created_at}', '{memo}')"
        self.cur.execute(sql)
        self.conn.commit()


if __name__ == "__main__":
    db = Mysql(host=HOST, passwd=PASS, db_name="test")
    db.save("2021-01-31", "testdayo-")
    print(db.load())
